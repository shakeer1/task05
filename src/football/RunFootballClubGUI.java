package football;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class RunFootballClubGUI extends JFrame {
    private JPanel contentPane;

    public RunFootballClubGUI() {
        setResizable(false);
        setTitle("Football Club League");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(750, 750);
        setLocationRelativeTo(null);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JLabel title = new JLabel("<html><h1><center><strong><i>Football Club League 2024</i></strong></center></h1><hr></html>");
        title.setBounds(150, 20, 500, 150);
        contentPane.add(title);

        JButton league1 = new JButton("Football Club League 01");
        league1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                football01.main(null);
            }
        });
        league1.setBounds(200, 200, 300, 50);
        contentPane.add(league1);

        JButton league2 = new JButton("Football Club League 02");
        league2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                football02.main(null);
            }
        });
        league2.setBounds(200, 250, 300, 50);
        contentPane.add(league2);

        JButton league3 = new JButton("Football Club League 03");
        league3.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                football03.main(null);
            }
        });
        league3.setBounds(200, 300, 300, 50);
        contentPane.add(league3);

        JButton league4 = new JButton("Football Club League 04");
        league4.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                football04.main(null);
            }
        });
        league4.setBounds(200, 350, 300, 50);
        contentPane.add(league4);

        JButton league5 = new JButton("Football Club League 05");
        league5.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                football05.main(null);
            }
        });
        league5.setBounds(200, 400, 300, 50);
        contentPane.add(league5);

        JButton exit = new JButton("Exit");
        exit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        exit.setBounds(200, 450, 300, 50);
        contentPane.add(exit);
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    RunFootballClubGUI frame = new RunFootballClubGUI();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}

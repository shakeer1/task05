package football;

import java.util.Arrays;
import java.util.List;

public class football04 {
    public static void main(String[] args) {
        List<Club> table = Arrays.asList(
                new Club(1, "FC Barcelona", 22, 16, 3, 3, 60, 25, 35, 30, 2, 150, 20, 10, 51),
                new Club(2, "Real Madrid", 22, 15, 4, 3, 55, 30, 25, 28, 4, 140, 15, 5, 49),
                new Club(3, "Manchester United", 22, 14, 5, 3, 50, 28, 22, 25, 3, 130, 18, 8, 47),
                new Club(4, "Liverpool FC", 22, 14, 3, 5, 58, 32, 26, 29, 5, 135, 16, 6, 45),
                new Club(5, "Bayern Munich", 22, 14, 2, 6, 55, 35, 20, 27, 6, 125, 14, 7, 44),
                new Club(6, "Paris Saint-Germain", 22, 12, 4, 6, 52, 40, 12, 24, 8, 110, 12, 9, 40),
                new Club(7, "Juventus FC", 22, 11, 5, 6, 48, 38, 10, 22, 9, 105, 10, 10, 38),
                new Club(8, "Manchester City", 22, 10, 6, 6, 45, 35, 10, 20, 10, 100, 8, 12, 36),
                new Club(9, "Chelsea FC", 22, 9, 7, 6, 40, 35, 5, 19, 11, 95, 6, 14, 34),
                new Club(10, "AC Milan", 22, 8, 6, 8, 38, 38, 0, 18, 12, 90, 5, 16, 32),
                new Club(11, "Arsenal FC", 22, 7, 8, 7, 35, 40, -5, 16, 14, 85, 4, 18, 29),
                new Club(12, "Borussia Dortmund", 22, 6, 7, 9, 30, 45, -15, 15, 16, 80, 2, 20, 25)
        );

        System.out.println("Several clubs have 10 points");
        table.stream().filter(club -> club.getPoints() == 10)
                .forEach(System.out::println);

        System.out.println();
        System.out.println("Clubs with 20 points");
        table.stream().filter(club -> club.getPoints() == 20)
                .forEach(System.out::println);
    }
}

package files;

import java.io.*;

/******************************************************************************
 * This program counts the number of lines in a text file.
 * 
 * @author Dr Kevan Buckley, University of Wolverhampton, 2019
 ******************************************************************************/

public class Files02 {

  public static void main(String[] args) throws Exception {
    BufferedReader bf  = 
      new BufferedReader(new FileReader("src/wolf-fox.txt"));

    System.out.println(bf.lines().count());

    bf.close();
  }

}

package files;

import java.io.BufferedReader;
import java.io.FileReader;

/******************************************************************************
 * This code shows how to process all lines of a text file.
 * 
 * @author Dr Kevan Buckley, University of Wolverhampton, 2019
 ******************************************************************************/

public class Files01 {

  public static void main(String[] args) throws Exception {
    BufferedReader bf = new BufferedReader(new FileReader("data/wolf-fox.txt"));

    bf.lines().forEach(l -> System.out.println(l));

    bf.close();
  }

}
